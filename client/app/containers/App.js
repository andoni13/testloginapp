import React, {Component} from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actionCreators from '../actions/actionCreators'

import { withRouter, Route } from 'react-router-dom'

import Dashboard from '../components/Dashboard'
import Home from '../components/Home'
import Modal from '../components/Modal'
import Topbar from '../components/Topbar'

class App extends Component {

	render() {
		return (
			<div>
				<Topbar {...this.props} />
				<Route path="/" exact render={()=><Home {...this.props} /> } />
				<Route path="/dashboard" render={()=><Dashboard {...this.props} />} />
			</div>
		)
	}
}

function mapDispatchToProps(dispatch) {
	return bindActionCreators(actionCreators, dispatch);
}

export default withRouter(connect((state) => {return state}, mapDispatchToProps)(App))