import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import FontAwesome from 'react-fontawesome'

const publicRoutes = ['Empresa', 'Productos', 'FAQ', 'Contacto']
const privateRoutes = ['dashboard']
let	  button 

export default class Topbar extends Component {

	componentWillUpdate(props) {

		let mobileMenu = this.refs.mobileMenu

		if(props.menu)
			mobileMenu.classList.add('toggle-mobile-menu') //Add class toggle-mobile-menu when toggle menu prop is true
		else if(!props.menu)
			mobileMenu.classList.remove('toggle-mobile-menu') //Remove class toggle-mobile-menu when toggle menu prop is false
	}

	logout() {
		this.props.fakeAuth(this.props.auth)
	}

	openModal () {
		this.props.toggleModal(this.props.modal)
	}

	menuRoutes(route, key) {
		return (
			<li key={key}>
				<Link to={`/${route}`}>{route}</Link>
			</li>
		)
	}

	render() {

		if(this.props.auth)
			button = <button className="cool-button button" onClick={this.logout.bind(this)}>Cerrar Sesión</button>
		else if(!this.props.auth)
			button = <button className="cool-button button" onClick={this.openModal.bind(this)}>Iniciar Sesión</button>

		return (
			<div className="topbar"> 
				<div className="container flex space-between vertical-align topbar-wrapper">
					<FontAwesome name='bars' className="toggle-button" onClick={()=> this.props.toggleMenu(this.props.menu)} />
					<Link className="logo-container" to="/">
						<img src="/img/logo.png" className="logo" alt="Super Cool Logo" />
					</Link>
					{/*** Desktop menu, only shown > 992px ***/}
					<nav className="desktop-menu flex horizontal-start vertical-align">
						{ (!this.props.auth) ? publicRoutes.map(this.menuRoutes) : privateRoutes.map(this.menuRoutes) }
						<li>
							{button}
						</li>
					</nav>
				</div>
				{/*** Mobile sidebar menu, only shown < 992px ***/}
				<div className="mobile-menu" ref="mobileMenu">
					<nav className="mobile-menu-wrapper flex horizontal-start vertical-items">
						{ (!this.props.auth) ? publicRoutes.map(this.menuRoutes) : privateRoutes.map(this.menuRoutes) }
						<li>
							{button}
						</li>
					</nav>
				</div>
			</div>
		)
	}
}

