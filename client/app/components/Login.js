import React, { Component } from 'react'
import validate from '../lib/validator'
import auth from '../lib/auth'
import message from '../lib/messages'
import CurrentDate from '../lib/date'

export default class Login extends React.Component {

 	componentWillUpdate(prop) {
 		//Only if modal prop is true
 		if(!prop.modal) {
 			this.clearForm()
 		}

 		if(prop.auth) {
 			this.props.history.push('/dashboard') //Redirect to dashboard view
 		}
 	}

	handleForm() {
		let email = validate.requiredValue(this.refs.email.value, this, 'Correo'),
			password = validate.requiredValue(this.refs.password.value, this, 'Contraseña')

		if(email !== undefined && password !== undefined) {
			let params = {username: email, password: password, log: CurrentDate}
			this.props.login('api/auth/login', params) //login user
			this.props.setLog('api/auth/log', params) //update login time
			
		}
	}

	clearForm() {
		this.refs.loginForm.reset() //reset form data
		this.props.retrieveMessage('') //clear the validation message
	}

	render() {
		return (
			<div>
				<form onSubmit={(e)=> e.preventDefault()} ref="loginForm">
					<fieldset>
						<div className="login-form">
							<p className="form-row flex horizontal-start vertical-align">
								<label htmlFor="email-input">Email</label>
								<input type="email" id="email-input" placeholder="correo@correo.com" ref="email" />
							</p>
							<p className="form-row flex horizontal-start vertical-align">
								<label htmlFor="email-input">Password</label>
								<input type="password" id="password-input" placeholder="contraseña" ref="password" />
							</p>
						</div>
						<div className="login-form-footer flex horizontal-start vertical-items">
							<div className="validation-message">
								<span>{this.props.message}</span>
							</div>
							<button className="cool-button button" onClick={this.handleForm.bind(this)}>Ingresar</button>
						</div>
					</fieldset>
				</form>
			</div>
		)
	}
}