import React, { Component } from 'react'
import * as _CONST_ from '../lib/consts'

export default class Dashboard extends Component {

	componentWillMount() {
		(!this.props.auth)&&this.props.history.push('/')//Prevent rendering the view if user is not 'fakely' authenticated
	}

	componentWillUpdate(prop) {
		if(prop.auth !== this.props.auth) //Only if auth prop changes
			(!prop.auth)&&this.props.history.push('/')//Prevent rendering the view if user is not 'fakely' authenticated
	}

	renderLog(log, key) {
		return(
			<div className="content-box-row" key={key}>
				{log}
			</div>
		)
	}

	render() {
		return (
			<div className="dashboard-wrapper">
				<div className="container"> 
					<div className="dashboard-inner-wrapper flex top-align horizontal-start space-between">
						<div className="content-box content-box-4">
							<div className="content-box-title text-center">
								ÚLTIMAS DE SESIONES
							</div>
							<div className="content-box-table text-center">
								{(this.props.user.length > 0)&&this.props.user.map(this.renderLog)}
							</div>
						</div> 
						<div className="content-box content-box-8 session-tracker-box">
							<div className="content-box-title text-center">
								SESIÓN ACTUAL
							</div>
							<div className="content-box-body flex center vertical-align counter">
								00:00:00
							</div>
						</div> 
					</div>
				</div>
			</div>
		) 
	}
}

