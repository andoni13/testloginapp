import React, {Component} from 'react'
import FontAwesome from 'react-fontawesome'

export default class Modal extends React.Component {

 	componentWillUpdate(prop) {
 		//Only if modal prop is true
 		if(prop.modal) {
 			this.openModal()
 		}

 		if(!prop.modal) {
			this.close()
 		}
 	}

 	close() {
		this.refs.modal.classList.add('close-modal') //close modal

		setTimeout(function() {
			this.refs.modal.classList.remove('open-modal') //remove any existing open-modal class
			this.refs.modal.classList.remove('close-modal') //clean close-modal class after animation is completed
		}.bind(this), 250)
 	}
 	
	closeModal(e) {
		//Bubbling event capture, to avoid closing the modal when clicking on modal-box
		if(e.nativeEvent.target.getAttribute('data-modal') === 'close') {
			this.props.toggleModal(true) //change modal state
			this.close()
		}
	}

	openModal() {
		this.refs.modal.classList.remove('close-modal') //remove any existing close-modal class
		this.refs.modal.classList.add('open-modal') //add open-modal class to modal overlay
	}

	render() {
		return (
			<div className="modal-overlay vertical-align center" ref="modal" data-modal="close" onClick={this.closeModal.bind(this)}>
				<div className="modal-box flex vertical-items">
					<FontAwesome name="close" className="close-button" data-modal="close" onClick={this.closeModal.bind(this)} />
					<div className="modal-container">
						{this.props.children}
					</div>
				</div>
			</div>
		)
	}
}