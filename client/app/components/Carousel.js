import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group'
import ReactDOM from 'react-dom'

let interval;

export default class Carousel extends React.Component{

 	componentWillUpdate(prop) {
 		if(prop.modal)
 			clearInterval(interval)//clear interval when modal opens

 		if(this.props.modal)
 			interval = setInterval(this.slide.bind(this), 6000)//resume interval when modal closes
 	}

	componentDidMount() {
		interval = setInterval(this.slide.bind(this), 6000)
	}

	componentWillUnmount() {
		clearInterval(interval)
	}

	slide() {
		let nextSlide = this.props.carouselCounter + 1 < this.props.slides.length ? this.props.carouselCounter + 1 : 0;
		this.props.slide(nextSlide)
	}
  
  	render(){

		let style = {
			backgroundImage : 'url(' + this.props.slides[this.props.carouselCounter] + ')'
		}

	    return(
			<div className="carousel">
				<ReactCSSTransitionGroup transitionName={'translate'} transitionEnterTimeout={2000} transitionLeaveTimeout={2000}>
					<div className="carousel-images" style={style} key={this.props.carouselCounter}></div>
				</ReactCSSTransitionGroup>
			</div>
	    )
  	}
}