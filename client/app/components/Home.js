import React, { Component } from 'react'

import Carousel from './Carousel'
import Modal from './Modal'
import Login from './Login'

let slides = [
		'http://i.imgur.com/TaA1gj9.png',
		'http://s3.amazonaws.com/libapps/accounts/61306/images/BoroughMarketTomatoes.jpg',
		'https://static.pexels.com/photos/96380/pexels-photo-96380.jpeg'
	]

export default class Home extends Component {

	render() {
		return (
			<div>
				<Modal {...this.props}> 
					<Login {...this.props} />
				</Modal>
      			<Carousel slides={slides} carouselCounter={this.props.carouselCounter} slide={this.props.slide} modal={this.props.modal} />
			</div>
		)
	}
}