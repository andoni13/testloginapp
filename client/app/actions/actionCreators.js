import Api from '../lib/api'
import Message from '../lib/messages'

//Actions
export function fakeAuth(bool) {
	return {
		type: 'FAKE_AUTH',
		bool
	}
}

export function login(url, params) {
	return (dispatch, getState) => {
		return Api.post(`/${url}`, params).then(resp => {
			dispatch(retrieveUserInfo({resp}))
			dispatch(fakeAuth(false))
			dispatch(toggleModal(true))
		}).catch((ex) => {
			dispatch(retrieveMessage(Message.retrieve(5)))
		})
	}
}

export function retrieveMessage(message) {
	return {
		type: 'RETRIEVE_MESSAGE', 
		message
	}
}

export function retrieveUserInfo({resp}) {
	return {
		type: "FETCH_USER_INFO",
		resp
	}
}

export function setLog(url, params) {
	return (dispatch, getState) => {
		return Api.put(`/${url}`, params).then(resp => {
			dispatch(retrieveUserInfo({resp}))
		}).catch((ex) => {
			console.log(ex);
		})
	}
}

export function slide(counter) {
	return {
		type: 'CAROUSEL_COUNTER', 
		counter
	}
}

export function toggleMenu(bool) {
	return {
		type: 'TOGGLE_MENU', 
		bool
	}
}

export function toggleModal(bool) {
	return {
		type: 'TOGGLE_MODAL', 
		bool
	}
}