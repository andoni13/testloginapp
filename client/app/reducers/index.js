import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { carouselCounter, 
		 menu, 
		 message, 
		 modal 
		} from './ui'
import { auth, user } from './auth'

const reducers = combineReducers({ auth, carouselCounter, menu, message, modal, user });

export default reducers;