//UI Reducer's
export function carouselCounter(state = [], action) {
	switch(action.type) {
		case 'CAROUSEL_COUNTER':
			return action.counter
		default:
			return state;
	}
}

export function menu(state = [], action) {
	switch(action.type) {
		case 'TOGGLE_MENU':
			if(action.bool) 
				return false;
			else 
				return true;
		default:
			return state;
	}
}

export function message(state = [], action) {
	switch(action.type) {
		case 'RETRIEVE_MESSAGE':
			return action.message
		default:
			return state;
	}
}

export function modal(state = [], action) {
	switch(action.type) {
		case 'TOGGLE_MODAL':
			if(action.bool) 
				return false;
			else 
				return true;
		default:
			return state;
	}
}