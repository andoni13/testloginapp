//Auth Reducer's
export function auth(state = [], action) {
	switch(action.type) {
		case 'FAKE_AUTH':
			if(action.bool)
				return false;
			else
				return true;
		default:
			return state;
	}
}

export function user(state = [], action) {
	switch(action.type) {
		case 'FETCH_USER_INFO':
			return action.resp;
		default:
			return state;
	}
}