import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import store from './store'
import { BrowserRouter as Router } from 'react-router-dom' // Import React Router deps

import css from './styles/app.styl' //Import CSS
import App from './containers/App'

const routes = (
	<Provider store={store}>
		<Router>
			<App />
		</Router>
	</Provider>
)

render(routes, document.querySelector('#root'))