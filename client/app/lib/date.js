let currentDate = new Date()
let day = currentDate.getDate()
let months = [	'Enero', 
				'Febrero', 
				'Marzo', 
				'Abril', 
				'Mayo', 
				'Junio', 
				'Julio', 
				'Agosto', 
				'Septiembre', 
				'Octubre', 
				'Noviembre', 
				'Diciembre'
			];
let month = months[currentDate.getMonth()]
let year = currentDate.getFullYear()
let hour = currentDate.getHours()
let minutes = currentDate.getMinutes()

if(day < 10)
    day = '0'+ day
if(month < 10)
    month = '0'+ month

exports.currentDate = `${day} de ${month}, ${hour}:${minutes}`