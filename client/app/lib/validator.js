import message from './messages';

module.exports = {
	requiredSizedValue(val, context, maxSize, input) {
		if(!val) 
			context.props.retrieveMessage(message.retrieve(1, context, input));
		else if(val.length <= maxSize) 
				return val;
		else 
			context.props.retrieveMessage(message.retrieve(2, context, input, maxSize));
	}, 
	requiredNumberValue(val, context, input) {
		if(!val)
			context.props.retrieveMessage(message.retrieve(1, context, input));
		else if(!isNaN(val))
			return val;
		else 
			context.props.retrieveMessage(message.retrieve(3, context, input));
		
	}, 
	requiredValue(val, context, input) {
		if (!val) 
			context.props.retrieveMessage(message.retrieve(1, context, input));
		else 
			return val;
	}
};