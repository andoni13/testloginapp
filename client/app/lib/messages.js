import React from 'react';

module.exports = {
	retrieve(id, context, input, maxSize) {
		switch(id) {
			case 0:
				return <div className="products-message">No se pudo encontrar el producto</div>;
				break;
			case 1:
				return `*Por favor ingesar ${input}`;
				break;
			case 2:
				return `*El texto de ${input} no puede tener más de ${maxSize} caractéres`;
				break;
			case 3:
				return `*El ${input} debe ser un valor numerico sin comas ni puntos`;
				break;
			case 4:
				return `*Correo electrónico inválido, por favor vuelva a ingresar su correo`;
				break;
			case 5:
				return `*Usuario y/o contraseña incorrectas, por favor vuelva a ingresar sus credenciales`;
				break;
		}
	}
}