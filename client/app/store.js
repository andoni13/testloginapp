import { createStore, compose, applyMiddleware, combineReduxers } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import reducers from './reducers/index'; // Import the reducers

const loggerMiddlerware = createLogger({ predicate: (getState, action) => global.__DEV__});

// Create an object for the default data
const defaultState = {
  menu: false,
  modal: false,
  carouselCounter: 0, 
  message: '', 
  auth: false, 
  user: []
}

function configureStore(initialState) {
  const enhancer  = compose(
    applyMiddleware(
      thunkMiddleware,
      loggerMiddlerware
    ),
  )
  return createStore(reducers, initialState, enhancer);
}

const store = configureStore(defaultState);

if(module.hot) {
	module.hot.accept('./reducers/', () =>{
		const nextRootReducer = require('./reducers/index').default;
		store.replaceReducer(nextRootReducer);
	})
}


export default store;