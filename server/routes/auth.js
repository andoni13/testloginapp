import express from 'express'
import bodyParser from 'body-parser'
import User from '../models/user'

const router = express.Router();

router.use(bodyParser.json());

router.route('/login')

.post(function (req, res, next) {
    User.findOne({username:req.body.username, password:req.body.password}, function (err, user) {
        (user) ? res.status(200).send(user.log) : res.status(403).send({error:'Acceso denegado'});
    });
})

router.route('/log')

.put(function (req, res, next) {
	User.update({username:req.body.username}, {$push: {log: req.body.log.currentDate}}, function(err, users) {
		User.findOne({username:req.body.username, password:req.body.password}, function (err, user) {
        	(user) ? res.status(200).send(user.log) : res.status(403).send({error:'Acceso denegado'});
    	});
	});
})

module.exports = router;