import mongoose from 'mongoose'
const Schema = mongoose.Schema

const userSchema = new Schema({
    username: {
    	type: String,
    	required: true
    },
    password: {
        type: String,
        required: true
    }, 
    log: {
        type: Array
    }
})

let user = mongoose.model('User', userSchema);

module.exports = user;