/*** Imports ***/
import path from 'path'
import express from 'express'
import webpack from 'webpack'
import webpackConfig from '../webpack.config.dev'
import mongoose from 'mongoose'
import bodyParser from 'body-parser'
import db from './config/db'
import auth from './routes/auth'

const compiler = webpack(webpackConfig)
const app = express()

/*** Webpack Configuration ***/

app.use(require('webpack-dev-middleware')(compiler, {
  noInfo: true,
  publicPath: webpackConfig.output.publicPath
}));

app.use(require('webpack-hot-middleware')(compiler));

/*** Routes Requires ***/

const appRoutes = ['/', '/dashboard'] //store all available routes for the application

/*** Routes Configuration ***/

app.get(appRoutes, function(req, res) {
  res.sendFile(path.join(__dirname, '../client/index.html'));
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname, 'public')));

app.use('/api/auth', auth);

/*** View engine setup ***/

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

/*** Web Server ***/

app.listen(7770, 'localhost', function(err) {
  if (err) {
    console.log(err);
    return;
  }

  console.log('Listening at http://localhost:7770');
});

/*** Catch 404 and forward to error handler ***/

app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

/*** Development error handler, will print stacktrace ***/

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

/*** Production error handler, no stacktraces leaked to user ***/

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});