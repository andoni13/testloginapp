# LOGIN TEST APP #

A simple login demo using NodeJS + MongoDB + React + Redux. 

	- To login use the credentials: 
	- username: admin@admin.com
	- password: 1234

### What is this repository for? ###

The only purpose of this project is to deliver a test for a job application.

# App Prerequisites

	- Node.js
	- npm
	
# Running

* First `npm install` to grab all the necessary dependencies. 
* Then run `npm run mongo` to run mongodb at <localhost:27017>.
* Then run `npm start` and open <localhost:7770> in your browser.

# Production Build

* Run `npm build` to create a distro folder and a bundle.js file.

### Who do I talk to? ###

* andoni282@gmail.com