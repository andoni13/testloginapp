var path = require('path');
var webpack = require('webpack');

module.exports = {
  devtool: 'source-map',
  entry: [
  'webpack-hot-middleware/client',
    './client/app/index.js'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin()
  ],
  module: {
    loaders: [
    // js
    {
      test: /\.js$/,
      loaders: ['babel-loader'],
      include: path.join(__dirname, 'client/app')
    },
    // CSS
    { 
      test: /\.styl$/, 
      include: path.join(__dirname, 'client/app'),
      loader: 'style-loader!css-loader!stylus-loader'
    },
    // URL files
    {
      test: /\.(png|woff|woff2|eot|ttf|svg|jpg)$/,
      use: [
       { loader: 'url-loader', options: { limit: 100000 } } 
      ]
    }
    ]
  }
};
